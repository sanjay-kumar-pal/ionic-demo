import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProjectsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-projects',
  templateUrl: 'projects.html',
})
export class ProjectsPage {
  projects: Array<{name: string, oppid: any, koneid: any,customer: string}>;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  this.projects = [];
  for (let i = 1; i < 20; i++) {
  this.projects.push({
  name: 'Project '+ i,
  oppid: 'OPP-' + i,
  koneid: 'KONE-' + i,
  customer: 'CUST' + i,
  });
  }
  
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 0; i < 20; i++) {
        this.projects.push( {
  name: 'Project '+ i,
  oppid: 'OPP-' + i,
  koneid: 'KONE-' + i,
  customer: 'CUST' + i,
  } );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectsPage');
  }

}
